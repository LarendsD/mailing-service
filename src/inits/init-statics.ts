import { mkdirSync } from 'fs';
import { join } from 'path';

export const initStatics = () => {
    const mailsArchivesPath = join(process.cwd(), 'fssp_mails', 'mails');
    const mailsPdfsPath = join(process.cwd(), 'fssp_mails', 'executive_documents');

    mkdirSync(mailsArchivesPath, { recursive: true });
    mkdirSync(mailsPdfsPath, { recursive: true });

    return {
        mailsArchivesPath,
        mailsPdfsPath,
    }
}