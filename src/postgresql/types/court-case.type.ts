/**
 * Реестр судебных производств
 */
export type CourtCase = {
    /**
     * Идентификатор
     */
    id?: number
    /**
     * Дата создания записи о судебном производстве
     */
    created_at?: Date
    /**
     * Идентификатор дела (в системе)
     */
    lawsuit_id: number
    /**
     * Код судебного участка
     */
    court_code: string
    /**
     * Номер дела (в суде)
     */
    case_number?: string
    /**
     * Ссылка на дело на сайте суда
     */
    external_url?: string
    /**
     * Сумма начислений присужденная
     */
    debt_charges_awarded?: number
    /**
     * Сумма пени присужденная
     */
    debt_fines_awarded: number | null
    /**
     * Сумма представительских расходов присужденная
     */
    representative_expenses_awarded?: number
    /**
     * Сумма почтовых расходов присвоенная
     */
    postal_expenses_awarded?: number
    /**
     * Общая сумма долга присужденная
     */
    total_awarded?: number
    /**
     * Первый месяц присужденного периода формирования долга
     */
    debt_period_start_awarded?: Date
    /**
     * Последний месяц присужденного периода формирования долга
     */
    debt_period_end_awarded?: Date
    /**
     * Сумма начислений истребуемая
     */
    debt_charges_claimed?: number 
    /**
     * Сумма пени истребуемая
     */
    debt_fines_claimed?: number
    /**
     * Сумма представительских расходов истребуемая
     */
    representative_expenses_claimed?: number
    /**
     * Сумма почтовых расходов истребуемая
     */
    postal_expenses_claimed?: number
    /**
     * Общая сумма долга истребуемая
     */
    total_claimed?: number
    /**
     * Первый месяц периода формирования долга заявленный
     */
    debt_period_start_claimed?: Date 
    /**
     * Последний месяц периода формирования долга заявленный
     */
    debt_period_end_claimed?: Date 
    /**
     * Сумма госпошлины присужденная
     */
    court_fee_awarded: number | null
    /**
     * Сумма госпошлины истребуемая
     */
    court_fee_claimed?: number
    /**
     * Уровень судебной инстанции по делу
     */
    instance_id: number
    /**
     * 
     */
    is_last: boolean
  }