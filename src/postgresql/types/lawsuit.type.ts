export type Lawsuit = {
    id: number
    court_case_id: number,
    createdAt: Date
    debtPeriodStart: Date | null
    debtPeriodEnd: Date | null
    debtPeriodsCount: number | null
    isValid: boolean
    invalidationReason: string | null
    accountId: number | null
    realtyId: number
    realtyShare: string
    egrnExtractFileGuid: string | null
    isSolidary: boolean
    isAgainstIp: boolean
    note: string | null
}