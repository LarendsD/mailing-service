import * as pg from 'pg';

export const pgClient = new pg.Client({
    connectionString: process.env.DATABASE_URL,
});

pgClient.connect();