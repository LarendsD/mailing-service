import { ImapFlow } from 'imapflow';
import { parseMails } from './helpers';
import { schedule } from 'node-cron';

const client = new ImapFlow({
    host: process.env.IMAP_HOST ?? '',
    port: Number(process.env.IMAP_PORT) ?? 994,
    secure: true,
    auth: {
        user: process.env.IMAP_USER ?? '',
        pass: process.env.IMAP_PASS ?? '',
    },
});

const main = async () => {
    await client.connect();
    await client.mailboxOpen('INBOX', { readOnly: true });

    schedule('00 00 3 * * 0-6', async () => {
        await parseMails(client, `1:*`);
    })
    client.addListener('exists', async ({ count, prevCount }) => {
        await parseMails(client, `${prevCount + 1}:${count}`);
    });
};

main().catch(err => console.error(err));