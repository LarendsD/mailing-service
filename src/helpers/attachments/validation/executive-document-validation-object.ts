import { NewExecutiveDocument } from "../../../interfaces/new-executive-document.interface";

export const executiveDocumentValidationObject: {
    [k in keyof Partial<NewExecutiveDocument['IId']>]: Function
} = {
    IdDeloNo: (data: string) => {
        const regex = /^\d-\d{3,4}\/\d{4}$/gm;
        
        if (!data || !data.match(regex)) {
            return 'Номер документа';
        };

        return null;
    },
    IdDocDate: (data: string) => {
        const regex = /^\d{4}-\d{2}-\d{2}$/gm

        if (!data || !data.match(regex)) {
            return 'Дата документа';
        };

        return null;
    },
    Debtors: (data: NewExecutiveDocument['IId']['Debtors']) => {
        if (!data || data.length === 0) {
            return 'Должники';
        };
        const snilsRegex = /^\d{3}-\d{3}-\d{3} \d{2}$/gm;
        const dateRegex = /^\d{4}-\d{2}-\d{2}$/gm
        for (const { Fio, Snils, BirthDate } of data) {
            if (!Fio) {
                return 'ФИО одного из должников';
            }
            const [ name, surname, patronymic ] = Fio.split(' ');

            if (!(name && surname && patronymic)) {
                return 'ФИО одного из должников'
            };

            if (!BirthDate || !BirthDate.match(dateRegex)) {
                return 'Дата Рождения одного из должников';
            };

            if (!Snils || !Snils.match(snilsRegex)) {
                return 'Снилс одного из должников';
            };
        }

        return null;
    },
    PartSize: (data: string) => {
        const regex = /^\d{1,2}\/\d{1,2}$/gm;

        if (data && data.toString().match(regex)) {
            return null;
        };

        return 'Доля';
    },
    FirstMonthDebt: (data: string) => {
        const regex = /^\d{4}-\d{2}-\d{2}$/gm

        if (!data || !data.match(regex)) {
            return 'Первый месяц долга';
        };

        return null;
    },
    LastMonthDebt: (data: string) => {
        const regex = /^\d{4}-\d{2}-\d{2}$/gm

        if (!data || !data.match(regex)) {
            return 'Последний месяц долга';
        };

        return null;
    },
    DebtSum: (data: string) => {
        const num = Number(data);

        if (!data || Number.isNaN(num)) {
            return 'Сумма начислений';
        };

        return null;
    },
    DebtFines: (data: string) => {
        const num = Number(data);

        if (!data || Number.isNaN(num)) {
            return 'Сумма пени';
        };

        return null;
    },
    DebtFee: (data: string) => {
        const num = Number(data);

        if (!data || Number.isNaN(num)) {
            return 'Сумма пошлины';
        };

        return null;
    }
}

/*
номер дела по регулярке
дата дела на возможность приведения к дате
ФИО на возможность разделить на Ф И О
дата рождения на возможность приведения к дате
Снилс на соответствие регулярке
Или несколько ФИО, дат рождения и СНИЛСов для солидарного приказа
Размер доли на соответствие регулярке
начало периода на возможность приведения к дате
конец периода на возможность приведения к дате
сумма начислений на возможность приведения к числу
сумма пени на возможность приведения к числу
сумма пошлины на возможность приведения к числу
*/