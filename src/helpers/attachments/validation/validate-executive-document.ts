import { ProcessingStatusDocument } from "../../../enums/processing-status-document.enum";
import { NewExecutiveDocument } from "../../../interfaces/new-executive-document.interface";
import { pgClient } from "../../../postgresql";
import { executiveDocumentValidationObject } from "./executive-document-validation-object";

export const validateExecutiveDocument = async (
    data: NewExecutiveDocument['IId'], 
    xml: string, 
    letterId: number,
) => {
    const validationKeys = Object.keys(executiveDocumentValidationObject) as Array<keyof NewExecutiveDocument['IId']>;

    const validationResult = [];

    for (const key of validationKeys) {
        const validationFunction = executiveDocumentValidationObject[key];

        if (validationFunction) {
            const validationString = validationFunction(data[key]);
            if (validationString) {
                validationResult.push(validationString);
            }
        }
    }

    const validationString = validationResult.join(', ');

    if (validationString.length !== 0) {
        const executiveErrorDocument = {
            email_letter_id: letterId,
            xml,
            processing_status: ProcessingStatusDocument.VALIDATION_FAILURE,
            error_message: `Валидация не прошла по следующим полям: ${validationString}`,
        };

        await pgClient.query(`
            INSERT INTO fssp_mails_executive_documents (
                email_letter_id,
                xml,
                processing_status,
                error_message
            ) VALUES (
                $1, $2, $3, $4
            )`,
            Object.values(executiveErrorDocument)
        );

        return false;
    }
    
    return true;
}