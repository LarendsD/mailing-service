import { NewExecutiveDocument } from "../../interfaces/new-executive-document.interface";
import { pgClient } from "../../postgresql";
import { Person } from "../../postgresql/types/person.type";

export const setDebtors = async (executiveDocumentId: number, debtors: NewExecutiveDocument['IId']['Debtors']) => {
    for (const debtor of debtors) {
        const person: Person = (await pgClient.query(`
            SELECT * FROM persons WHERE snils = $1
        `, [debtor.Snils])).rows[0];

        const shareholder = (await pgClient.query(`
            SELECT * FROM fssp_shareholders WHERE snils = $1
        `, [debtor.Snils])).rows[0];

        const [surname, firstName, patronymic] = debtor.Fio.split(' ');

        if (person && !shareholder) {
            const shareholder = {
                surname,
                name: firstName,
                patronymic,
                birth_date: debtor.BirthDate,
                snils: debtor.Snils,
                citizenship: 'Российская Федерация',
                document_series: debtor.MvvDatumIdentificator.SerDoc,
                document_number: debtor.MvvDatumIdentificator.NumDoc,
                document_date: debtor.MvvDatumIdentificator.ActDate,
                document_issuer: debtor.MvvDatumIdentificator.IssuedDoc,
                document_type: '008001001000',
                executive_document_id: executiveDocumentId,
            }   

            await pgClient.query(`
                INSERT INTO fssp_shareholders (
                    surname,
                    name,
                    patronymic,
                    birth_date,
                    snils,
                    citizenship,
                    document_series,
                    document_number,
                    document_date,
                    document_issuer,
                    document_type,
                    executive_document_id
                ) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12)
            `, Object.values(shareholder));
        }
    }
}