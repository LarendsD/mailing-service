import { Readable } from "imapflow";
import * as fs from 'fs';
import { promisify } from 'util';
import { exec } from 'node:child_process';
import { join } from 'path';
import { ProcessingStatusDocument } from "../../enums/processing-status-document.enum";
import { pgClient } from "../../postgresql";
const execute = promisify(exec);

export const decryptAttachment = async (
    fileStream: Readable, 
    fileName: string, 
    tmpDirPath: string,
    staticPath: string,
    letterId: number,
) => {
    const encryptedFilePath = join(tmpDirPath, fileName);
    const decryptedFilePath = join(staticPath, `${fileName}.7z`);
    const writeStream = fs.createWriteStream(encryptedFilePath);
    const privateKeyPath = process.env.PRIVATE_KEY_PATH;
    const privateKeyPassword = process.env.PRIVATE_KEY_PASSWORD;

    for await (const chunk of fileStream) {
        writeStream.write(chunk);
    };

    const { stderr } = await execute(`
        openssl cms -decrypt -binary -in ${encryptedFilePath} -inkey ${privateKeyPath} -inform PEM -out ${decryptedFilePath} -passin pass:${privateKeyPassword}
    `);

    if (stderr.length) {
        console.error(stderr);
        const executiveErrorDocument = {
            email_letter_id: letterId,
            processing_status: ProcessingStatusDocument.DECRYPT_FAILURE,
            error_message: stderr,
        };

        await pgClient.query(`
            INSERT INTO fssp_mails_executive_documents (
                email_letter_id,
                processing_status,
                error_message
            ) VALUES (
                $1, $2, $3
            ) RETURNING id`,
            Object.values(executiveErrorDocument)
        );
        process.exit(1);
    };

    fs.rmSync(encryptedFilePath);

    return encryptedFilePath;
}