import { ProcessingStatusDocument } from "../../enums/processing-status-document.enum";
import { NewExecutiveDocument } from "../../interfaces/new-executive-document.interface";
import { pgClient } from "../../postgresql";
import { Lawsuit } from "../../postgresql/types/lawsuit.type";
import { updateCourtCase } from "./update-court-cases";

export const getStatus = async (data: NewExecutiveDocument['IId']) => {    
    const lawsuit: Lawsuit = (await pgClient.query(`
        SELECT courts_cases.id AS court_case_id, lawsuits.id FROM lawsuits
            JOIN courts_cases ON courts_cases.lawsuit_id = lawsuits.id
            WHERE courts_cases.case_number = $1
        `, [data.IdDeloNo]
    )).rows[0];

    const courtCaseData = {
        court_case_id: lawsuit?.court_case_id,
        lawsuit_id: lawsuit?.id,
    }

    if (lawsuit) {
        await updateCourtCase(lawsuit.court_case_id, data);

        return {
            processing_status: ProcessingStatusDocument.LINKED_TO_EXISTING_PERSON_FROM_EXISTING_CASE,
            ...courtCaseData,
        };
    }

    return {
        processing_status: ProcessingStatusDocument.LAWSUIT_NOT_FOUND,
        ...courtCaseData,
    }
}