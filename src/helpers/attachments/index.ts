import { XMLParser } from "fast-xml-parser";
import { ImapFlow, MessageStructureObject } from "imapflow";
import { pgClient } from "../../postgresql";
import { getStatus } from "./get-status";
import { decryptAttachment } from "./decrypt";
import { writeFileSync } from 'fs';
import { tmpdir } from 'os';
import { join } from 'path';
import { unzipAttachment } from "./unzip";
import { mkdirSync } from 'fs';
import { initStatics } from "../../inits/init-statics";
import { NewExecutiveDocument } from "../../interfaces/new-executive-document.interface";
import { validateExecutiveDocument } from "./validation/validate-executive-document";
import { setDebtors } from "./set-debtors";

export const setAttachmentData = async (
    client: ImapFlow, 
    attachment: MessageStructureObject, 
    messageUid: string, 
    letterId: number
) => {
    const file = await client.download(
        messageUid, 
        attachment.part, 
        { 
            uid: true,
        },
    );
    const tmpDirPath = join(`${tmpdir}`, 'pir-email-service');

    mkdirSync(tmpDirPath, { recursive: true });

    const statics = initStatics();

    // ВРЕМЕННО ДЛЯ ТЕСТОВ
    // НАЧАЛО
    const decryptedPath = join(statics.mailsArchivesPath, `${file.meta.filename}.7z`);

    let chunks = [];
    for await (const chunk of file.content) {
        chunks.push(chunk);
    }
    const bodyResult = Buffer.concat(chunks);

    writeFileSync(decryptedPath, bodyResult);

    // КОНЕЦ
    // Для тестов незашифрованный!
    /*const decryptedPath = await decryptAttachment(
        file.content, 
        file.meta.filename ?? '', 
        tmpDirPath, 
        statics.mailsArchivesPath, 
        letterId,
    );*/

    const result = await unzipAttachment(
        decryptedPath, 
        tmpDirPath, 
        statics.mailsPdfsPath, 
        letterId, 
        attachment.id
    );

    if (result.xmls.length === 0) {
        throw new Error('xml в архиве не найдены!')
    }
    
    const parser = new XMLParser({ ignoreAttributes: true, isArray: (tagName) => tagName === 'Debtors' });
    const { IId }: NewExecutiveDocument = parser.parse(result.xmls[0].content);

    const validationPassed = await validateExecutiveDocument(IId, result.xmls[0].content, letterId);

    console.log(validationPassed);
    if (!validationPassed) {
        return;
    }

    const executiveDocument = {
        email_letter_id: letterId,
        xml: result.xmls[0].content,
        num: IId.IdDocNo,
        issue_date: IId.IdDocDate,
        court_case_num: IId.IdDeloNo,
        court_title: IId.WorldCourtName,
        clarification_text: IId.Context,
        total_claims: IId.DebtSum + IId.DebtFines +IId.DebtFee,
        claims_discovered_charges: IId.DebtSum,
        claims_discovered_fines: IId.DebtFines,
        claims_discovered_fee: IId.DebtFee,
        claims_discovered_period_start: IId.FirstMonthDebt,
        claims_discovered_period_end: IId.LastMonthDebt,
        ...await getStatus(IId),
    }

    try {
        await pgClient.query('BEGIN')

        const { id } = (await pgClient.query(`
            INSERT INTO fssp_mails_executive_documents (
                email_letter_id, 
                xml,
                num, 
                issue_date,
                court_case_num,
                court_title,
                clarification_text,
                total_claims,
                claims_discovered_charges,
                claims_discovered_fines,
                claims_discovered_fee,
                claims_discovered_period_start,
                claims_discovered_period_end,
                processing_status,
                court_case_id,
                lawsuit_id
            ) VALUES (
                $1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15, $16
            ) RETURNING id`,
            Object.values(executiveDocument)
        )).rows[0];

        await setDebtors(id, IId.Debtors);

        await pgClient.query('COMMIT')
    } catch (e) {
        await pgClient.query('ROLLBACK')
    }      
}