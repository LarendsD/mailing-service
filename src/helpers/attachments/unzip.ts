import { extractFull } from "node-7z"
import { join, parse, ParsedPath } from 'path';
import { readFileSync, writeFileSync, statSync } from 'fs';
import { v4 } from 'uuid';
import { ProcessingStatusDocument } from "../../enums/processing-status-document.enum";
import { pgClient } from "../../postgresql";
import { encrypt } from "../../common/encrypt";

type File = {
	fileinfo: ParsedPath;
	content: string;
}

export const unzipAttachment = async (
  attachmentPath: string, 
  tmpDirPath: string,
  staticPath: string,
  letterId: number,
  attachmentId: string,
) => {
    const { name } = parse(attachmentPath);
    const unzipPath = join(tmpDirPath, `${name}-dir`);

    const unzipStream = extractFull(attachmentPath, unzipPath, {
    	$cherryPick: ['ИД/*.xml', 'ИД/*.pdf'],
    });

    const extractedFiles: {
		xmls: Array<File>,
		pdfs: Array<File>
	} = {
		xmls: [],
		pdfs: [],
	};

    unzipStream.on('data', async (data) => {
    	if (data.status === 'extracted') {
    		const filePath = data.file;
        	const parsedFilePath = parse(filePath);

        	const content = readFileSync(filePath, { encoding: 'utf-8' });

			const dataToPush = {
				fileinfo: parsedFilePath,
            	content,
			}

			if (parsedFilePath.ext === '.pdf') {
				const sha1 = encrypt(content);
				const pdf = (await pgClient.query(`
					SELECT * FROM files WHERE sha1 = $1
				`, [sha1])).rows[0];

				if (!pdf) {
					const uuid = v4();
					const pathToPdf = join(staticPath, uuid);
					writeFileSync(pathToPdf, content);
					const { size } = statSync(pathToPdf);
	
					const pdfFile = {
						original_name: parsedFilePath.base,
						file_size: size,
						storage_path: pathToPdf,
						sha1: encrypt(content),
						file_guid: uuid,
					};
	
	
					await pgClient.query(`
						INSERT INTO files (
							original_name,
							file_size,
							storage_path,
							sha1,
							file_guid
						) VALUES ($1, $2, $3, $4, $5)
					`, Object.values(pdfFile));
				}
				extractedFiles.pdfs.push(dataToPush);
			} else {
				extractedFiles.xmls.push(dataToPush);
			}
      	}
    })
      
    unzipStream.on('progress', function (progress) {
    	console.log(`Current unzip progress: ${progress.percent}%`)
    })
      
    unzipStream.on('error', async (err) => {
    	console.error(err.message);
      	const executiveErrorDocument = {
         	email_letter_id: letterId,
          	attachment_id: attachmentId,
          	processing_status: ProcessingStatusDocument.EXTRACT_FAILURE,
          	error_message: err.message,
      	};

      	await pgClient.query(`
          	INSERT INTO fssp_mails_executive_documents (
        		email_letter_id, 
              	attachment_id,
              	processing_status,
              	error_message
          	) VALUES (
              	$1, $2, $3, $4
          	) RETURNING id`,
          	Object.values(executiveErrorDocument)
      	);
      	process.exit(1);
    })

	for await (const chunk of unzipStream) {
		console.log(chunk);
	}

    return extractedFiles;
}