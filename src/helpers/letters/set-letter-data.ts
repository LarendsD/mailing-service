import { FetchMessageObject, ImapFlow, MessageStructureObject } from "imapflow";
import { ProcessingStatusEmailLetter } from "../../enums/processing-status-email-letter.enum";
import { pgClient } from "../../postgresql";

export const setLetterData = async (
    client: ImapFlow,
    message: FetchMessageObject,
    docsCount: number,
): Promise<number> => {
    const bodyTextNode = message.bodyStructure.childNodes.shift();

    const { content } = await client.download(
        message.uid.toString(),
        bodyTextNode?.childNodes[0].part,
        {
            uid: true,
        }
    );
    let chunks = [];
    for await (const chunk of content) {
        chunks.push(chunk);
    }
    const bodyResult = Buffer.concat(chunks).toString("utf-8");

    const emailLetter = {
        sender: message.envelope.sender[0].address,
        message_uid: message.uid.toString(),
        text: bodyResult,
        recieving_date: message.internalDate,
        pir_recieving_date: null,
        docs_found: docsCount,
        docs_success: 0,
        docs_failure: 0,
        status: ProcessingStatusEmailLetter.IN_PROCESS,
    };

    const { rows } = await pgClient.query(`
        INSERT INTO fssp_mails (
            sender,
            message_uid,
            text, 
            recieving_date, 
            pir_recieving_date, 
            docs_found,
            docs_success, 
            docs_failure, 
            status
        ) VALUES (
            $1, $2, $3, $4, $5, $6, $7, $8, $9
        ) RETURNING id`,
        Object.values(emailLetter)
    );

    return rows[0].id;
}