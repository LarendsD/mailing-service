import { FetchQueryObject, ImapFlow } from "imapflow";
import { setLetterData } from "./letters/set-letter-data";
import { pgClient } from "../postgresql";
import { ProcessingStatusEmailLetter } from "../enums/processing-status-email-letter.enum";
import { ProcessingStatusDocument } from "../enums/processing-status-document.enum";
import { setAttachmentData } from "./attachments";

export const parseMails = async (client: ImapFlow, range: string) => {
    const messageQuery: FetchQueryObject = {
        envelope: true,
        bodyStructure: true,
        internalDate: true,
        uid: true,
    }
    const messages = [];
        for await (let message of client.fetch({ flagged: false, seq: range }, messageQuery)) {
            messages.push(message);
        };

        for (const message of messages) {
            const { childNodes } = message.bodyStructure;
            const attachments = childNodes.filter((childNode) => childNode.disposition === 'attachment');

            let letterId;
            try {
                letterId = await setLetterData(client, message, attachments.length);
            } catch (e: any) {
                console.log(`Error was occured while setting letter data: ${e.message}`);
                const resultingObject = {
                    sender: message.envelope.sender[0].address,
                    message_uid: message.uid.toString(),
                    recieving_date: message.internalDate,
                    pir_recieving_date: null,
                    docs_found: attachments.length,
                    docs_success: 0,
                    docs_failure: 0,
                    status: ProcessingStatusEmailLetter.FAILURE,
                    error_message: e.message, 
                }

                await pgClient.query(`INSERT INTO fssp_mails (
                        sender,
                        message_uid,
                        recieving_date,
                        pir_recieving_date,
                        docs_found,
                        docs_success,
                        docs_failure,
                        status,
                        error_message
                    ) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9)
                    RETURNING id
                `, Object.values(resultingObject));

                continue;
            }
            let docsSuccess = 0;
            let docsFailure = 0;

            for (const attachment of attachments) {
                try {
                    await setAttachmentData(client, attachment, message.uid.toString(), letterId);
                    docsSuccess += 1;
                } catch (e: any) {
                    const executiveErrorDocument = {
                        email_letter_id: letterId,
                        processing_status: ProcessingStatusDocument.SERVICE_FAILURE,
                        error_message: e.message.toString(),
                    };

                    await pgClient.query(`
                        INSERT INTO fssp_mails_executive_documents (
                            email_letter_id, 
                            processing_status,
                            error_message
                        ) VALUES (
                            $1, $2, $3
                        )`,
                        Object.values(executiveErrorDocument)
                    );
                    docsFailure += 1;
                }
            }

            await pgClient.query(`
                UPDATE fssp_mails SET status = $1, docs_success = $2, docs_failure = $3 WHERE id = $4
            `, [ProcessingStatusEmailLetter.SUCCESS, docsSuccess, docsFailure, letterId])
            await client.messageFlagsAdd(message.uid.toString(), ['\\Seen'], { uid: true })
        }
}