export enum ProcessingStatusDocument {
    /**
     * Исполнительный документ привязан к делу по имеющемуся собственнику
     */
    LINKED_TO_EXISTING_PERSON_FROM_EXISTING_CASE = 1,
    /**
     * Исполнительный документ привязан к делу, создан новый собственник
     */
    LINKED_TO_NEW_PERSON_FROM_EXISTING_CASE = 2,
    /**
     * Создано новое дело на основе исполнительного документа по имеющемуся собственнику 
     */
    LINKED_TO_EXISTING_PERSON_FROM_NEW_CASE = 3,
    /**
     * Создано новое дело на основе исполнительного документа, создан новый собственник
     */
    LINKED_TO_NEW_PERSON_FROM_NEW_CASE = 4,
    /**
     * Периоды пересекаются с имеющимся в системе делом
     */
    CASE_PERIODS_CROSSING = 5,
    /**
     * У должника несколько открытых дел, невозможно однозначно определить дело 
     */
    PERSON_HAVE_MANY_OPEN_CASES = 6,
    /**
     * Не удалось установить должника
     */
    PERSON_NOT_FOUND = 7,
    /**
     * Не удалось определить лицевой счёт объекта недвижимости
     */
    ACCOUNT_NOT_DEFINED = 8,
    /**
     * Нет такого лицевого счёта в `accounts`
     */
    ACCOUNT_NOT_FOUND = 9,
    /**
     * Ошибка дешифровки
     */
    DECRYPT_FAILURE = 10,
    /**
     * Ошибка деархивации
     */
    EXTRACT_FAILURE = 11,
    /**
     * Ошибка сервиса
     */
    SERVICE_FAILURE = 12,
    /**
     * Ошибка валидации
     */
    VALIDATION_FAILURE = 13,
    /**
     * Дело не найдено
     */
    LAWSUIT_NOT_FOUND = 14,
}