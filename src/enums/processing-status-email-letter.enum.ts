export enum ProcessingStatusEmailLetter {
    IN_PROCESS = 1,
    SUCCESS = 2,
    FAILURE = 3,
};
